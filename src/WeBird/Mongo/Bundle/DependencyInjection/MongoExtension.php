<?php
/**
 * User: mihard
 * Date: 26.09.14
 * Time: 14:37
 */

namespace WeBird\Mongo\Bundle\DependencyInjection;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class MongoExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if ($config['command_char'] != '$') {
            ini_set('mongo.cmd', $config['command_char']);
        }

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $container->addAliases(['webird.mongo.mapper' => $config['mapper']]);
        $container->setParameter('webird.mongo.connections', isset($config['connections']) ? $config['connections'] : []);
    }

    public function getAlias()
    {
        return 'webird_mongo';
    }
}
