<?php

namespace WeBird\Mongo\Bundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('webird_mongo');

        $rootNode->children()
                ->scalarNode('mapper')->defaultValue('webird.mongo.mapper.default')->end()
                ->scalarNode('command_char')->defaultValue('$')->end()

                ->arrayNode('connections')
                ->useAttributeAsKey('connection')->prototype('array')
                ->children()
                        ->scalarNode('dsn')->defaultValue('mongodb://localhost:27017')->end()
                        ->scalarNode('database')->end()
                        ->end()
                ->end()
        ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
