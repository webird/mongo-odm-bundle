<?php

namespace WeBird\Mongo\Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use WeBird\Mongo\Bundle\DependencyInjection\MongoExtension;

class WeBirdMongoBundle extends Bundle
{

    public function getContainerExtension() {

        if (null === $this->extension || !($this->extension instanceof MongoExtension)) {
            $this->extension = new MongoExtension();
        }

        return $this->extension;
    }

}
